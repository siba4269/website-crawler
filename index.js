#!/usr/bin/env node
process.env.UV_THREADPOOL_SIZE = 128;

const cheerio = require('cheerio');
const request = require('request-promise');
const async = require('async');

const root = "https://medium.com";
const linkStore = [root];

function start(url){	
	return getContent(url)
		.then(getLinks)
		.then(pushToQueue)
		.catch((err)=>console.error(err.message, err.options.uri));
}

var q = async.queue(function(link, callback) {
	//console.log('inside queue ', link);
	start(link).then(()=>callback()).catch(()=>callback());
}, 5);

q.drain = function() {
    console.log('all items have been processed');
    console.log('Total links: ', linkStore.length)
};

function pushToQueue(links){
	links.forEach((link)=>q.push(link));
}


function getContent(url){
	console.log('requesting ', url);
	return request({
		url,
		timeout: 1000 * 60
	})
}

// get all links
function getLinks(content){
	const $ = cheerio.load(content);
	
	const links = $('a').map( function() {
	    return $(this).attr('href');
	}).get();

	let formatedLinks = links.map((link)=>{
		return link.includes(root) || link.includes('//') ? link : root+link
	})
	
	return Promise.resolve(
		storeLinks(formatedLinks)
	);
}

function storeLinks(links){
	//console.log('links ',links);
	let unique = [];
	links.forEach((link)=>{
		// if not inside store
		if(!linkStore.includes(link)){
			linkStore.push(link);
			// if not a external link
			if(link.includes(root)){
				unique.push(link);	
			}			
		}
	})
	//console.log('unique ', unique);
	return unique;
}

// kick start
start(root);
