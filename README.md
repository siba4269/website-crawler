# Website Crawler

A simple crawler for website

## Getting Started

### Prerequisites

```
https://nodejs.org/en/
```

### Installing

install npm dependencies
```
npm install
```
## Running

```
npm start
```


## Built With

* [Nodejs](https://nodejs.org/en/)

## Authors

* **SIBA PRAKASH** - *siba4269@gmail.com* 
